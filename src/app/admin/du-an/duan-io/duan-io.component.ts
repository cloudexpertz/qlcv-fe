import { RoleConstants } from 'src/app/shared/constants';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProjectService } from 'src/app/services/project.service';
import { TeamAccountService } from 'src/app/services/team-account.service';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.services';
import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { ProgressService } from 'src/app/shared/services/progress-http.service';
import { validationAllErrorMessagesService } from 'src/app/shared/validators/validatorService';
import { RoleIoComponent } from '../../roles/role-io/role-io.component';

@Component({
  selector: 'app-duan-io',
  templateUrl: './duan-io.component.html',
  styleUrls: ['./duan-io.component.css']
})
export class DuanIoComponent implements OnInit {

  IOForm: FormGroup;
  public inputModel: any;
  public editMode: boolean;
  public purpose: string;
  public obj: any;
  dataModel: any;
  listGiamdoc: any;
  // error message
  validationErrorMessages = {
    name: { required: "Tên nhiệm vụ không được để trống!" },
  };

  // form errors
  formErrors = {
    name: "",
  };

  listLeader: any;
  listTeam: any;
  // ctor
  constructor(
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public imDialogService: MatdialogService,
    private projectService: ProjectService,
    private commonService: CommonServiceShared,
    public dialogRef: MatDialogRef<RoleIoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserService,
    private teamService: TeamService,
    private teamAccService: TeamAccountService
  ) { }

  // onInit
  async ngOnInit() {
    if (this.data.model && this.data.model.purpose) {
      this.purpose = this.data.model.purpose;
      this.dataModel = this.data.model
    }
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }

  // Lấy danh sách team

  async getTeam() {
    this.listTeam = await this.teamService.getFetch();
  }

  async getTeamMember(option?:string) {
    var idTeam = await this.IOForm.controls['idTeam'].value;

    this.listLeader = await this.teamAccService.getAll(idTeam);
    if(option === 'add') {
      let team = await this.teamService.getFetchById(idTeam);

      this.IOForm.controls['idLeader'].setValue(team.idLeader);
    }

  }

  // danh sách tài khoản là giám đóc/ phó giám đốc
  public async getListGiamdoc() {
    this.listGiamdoc = await this.userService.getAccountByRole(RoleConstants.NHOMONLYGIAMDOC);
  }

  // config Form use add or update
  bindingConfigAddOrUpdate() {

    // check edit
    if (this.dataModel) {
      this.getTeam();
      this.getListGiamdoc();
      this.IOForm.controls['idTeam'].setValue(this.dataModel.idTeam);
      this.getTeamMember('edit');
      this.IOForm.setValue({
        name: this.dataModel.name,
        start: this.dataModel.start,
        end: this.dataModel.end,
        idTeam: this.dataModel.idTeam,
        idLeader: this.dataModel.idLeader,
        idGiamdoc: this.dataModel.idGiamdoc
      });
    }
  }

  // config input validation form
  bindingConfigValidation() {

    this.getTeam();
    this.getListGiamdoc();
    this.IOForm = this.formBuilder.group({
      name: ["", Validators.required],
      start: [""],
      end: [""],
      idTeam: [""],
      idLeader: [""],
      idGiamdoc: [""],
    });
  }


  // on Submit
  public onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.IOForm.valid === true && !this.purpose) {
      this.inputModel = this.IOForm.value;
      this.inputModel.type = 1;
      this.projectService.addDuan(this.inputModel).subscribe(
        res => this.imDialogService.doParentFunction("getDuan"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Thêm mới dự án thành công!",
            2000
          )
      );
      this.imDialogService.doParentFunction("closeMatDialog");
    } if (this.IOForm.valid === true && this.purpose) {
      this.inputModel = this.IOForm.value;
      this.inputModel.id = this.dataModel.id;
      this.inputModel.type = 1;
      this.projectService.update(this.inputModel).subscribe(
        res => this.imDialogService.doParentFunction("getDuan"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Cập nhật dự án thành công!",
            2000
          )
      );
      this.imDialogService.doParentFunction("closeMatDialog");
    }


  }

  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.IOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // close sidebar
  public closeIOSidebar() {
    this.imDialogService.doParentFunction("closeMatDialog");
  }

}
