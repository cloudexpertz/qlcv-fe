import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaoCaoCongViecComponent } from './bao-cao-cong-viec.component';

describe('BaoCaoCongViecComponent', () => {
  let component: BaoCaoCongViecComponent;
  let fixture: ComponentFixture<BaoCaoCongViecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaoCaoCongViecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaoCaoCongViecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
