import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { CategoryService } from './../../services/category.service';
import { LinhVucIoComponent } from './linh-vuc-io/linh-vuc-io.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-linh-vuc',
  templateUrl: './linh-vuc.component.html',
  styleUrls: ['./linh-vuc.component.css']
})
export class LinhVucComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'name', 'action'];
  dataSource: any;

  listCategory: any = [];
  // dialog
  public mDialog: any;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private categoryService: CategoryService,
    private imDialog: MatDialog,
    public imDialogService: MatdialogService,
    public commonService: CommonServiceShared
  ) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  async ngOnInit() {
    await this.getCategory();


  }
  async getCategory() {
    this.listCategory = await this.categoryService.getFetchCategory();

      var data = this.listCategory.map((item, index) => {
        item.stt = index + 1;
      });
      this.dataSource = new MatTableDataSource(this.listCategory);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;

  }

  addCategory() {
    this.showPopup();
  }
  editCategory(item) {
    item.purpose = "edit";
    this.showPopup(item);
  }
  deleteCategory(item) {
    const dialogRef = this.commonService.confirmDeleteDiaLogService(
      "Tên lĩnh vực:",
      item.name
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.categoryService
          .delete(item.id).subscribe(
            () => this.getCategory(),
            (error: HttpErrorResponse) => {
              this.commonService.showError(error);
            },
            () => this.commonService.showeNotiResult("Đã xóa thành công: " + item.name, 2000)
          );
      }
    });
  }

  public doFilter = (value: string) => {

    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  /**
   * Mở popup
   */
  public showPopup(data?) {
    this.mDialog.setDialog(
      this,
      LinhVucIoComponent,
      "",
      "",
      data,
      "40%",
      "25vh",
      1
    );
    this.mDialog.open();
  }

  /**
   * Hàm đóng mat dialog
   */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  doFunction(methodName) {
    this[methodName]();
  }

}
