import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BaseRepositoryService } from './baserepository.service';


@Injectable({
    providedIn: 'root'
})
export class CategoryService {

    serviceName = "category";

    constructor(private repository: BaseRepositoryService) {
    }

    public  getFetchCategory() {
        var response =  this.repository.getFetchRepository(this.serviceName );
        return  response ;
    }

    // Thêm
    public insert(obj: any) {
        return this.repository.addRepository(this.serviceName, obj);
    }

    // Update
    public update(obj: any) {
        return this.repository.updateRepository(this.serviceName, obj);
    }

    public delete(id: any) {
        return this.repository.deleteRepository(this.serviceName+ '?id=' + id);
    }
}
