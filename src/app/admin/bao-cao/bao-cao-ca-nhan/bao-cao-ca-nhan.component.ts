import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { UserService } from 'src/app/services/user.services';
import { element } from 'protractor';
import { async } from '@angular/core/testing';
import { TeamAccountService } from 'src/app/services/team-account.service';
import { TeamService } from 'src/app/services/team.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { IssueService } from 'src/app/services/issue.service';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { RoleConstants } from 'src/app/shared/constants';
import { DetailTaskComponent } from '../../detail-task/detail-task.component';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';


export interface BaoCaoIssue {
  stt: string,
  title: string,
  dueDate: string,
  finishDate: string,
  coopId: string,
  idLeader: string,
  hqCao: string,
  hqDat: string,
  hqThap: string,
  hqChuadambao: string,
  tdVuot: string,
  tdDung: string,
  tdCham: string,
  nnKhachquan: string,
  nnDieuchinh: string,
  nnChuquan: string,
}


@Component({
  selector: 'app-bao-cao-ca-nhan',
  templateUrl: './bao-cao-ca-nhan.component.html',
  styleUrls: ['./bao-cao-ca-nhan.component.css']
})
export class BaoCaoCaNhanComponent implements OnInit {
  @ViewChild('TABLE', { static: true }) table: ElementRef;
  displayedColumns: string[] = ['position', 'name', 'dueDate', 'finsihDate', 'coop', 'leader', 'cao', 'dat', 'thap', 'chua-dam-bao', 'vuot', 'dung', 'cham', 'khach-quan', 'dieu-chinh', 'chu-quan', 'chaphanh_ot', 'chaphanh_ttbc'];
  dataSource: any;
  mode: any;
  // ngModel
  public nhanvien: any;
  public timeFrom: any;
  public timeTo: any;
  public team: any;
  public idLeader: any;

  public listNhanvien: any;
  public listIssue: any = [];
  public currentUser: any;
  public listAccount: any = [];
  public listTeam: any;
  public mDialog: any;
  constructor(
    public teamService: TeamService,
    public teamAccService: TeamAccountService,
    public datePipe: DatePipe,
    private issueService: IssueService,
    private userService: UserService,
    private imDialog: MatDialog,
    public imDialogService: MatdialogService,
    public commonService: CommonServiceShared
  ) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentAcc'));
    if (this.currentUser.role == RoleConstants.SU || this.currentUser.role == RoleConstants.GIAMDOC || this.currentUser.role == RoleConstants.PHUTRACHTONGHOP) {
      this.mode = 'su';
    };
    this.getListTeam();
    this.getListNhanvien();
    this.getListAccount();
  }

  // export table ra file excel
  ExportTOExcel() {
    this.commonService.ExportTOExcel(this.table, 'bao-cao-ca-nhan');
  }

  // danh sách nhân viên trong phòng ban
  public async getListNhanvien() {
    let team = await this.teamService.getFetchByLeader(this.currentUser.id);
    if (team)
    {
      this.listNhanvien = await this.teamAccService.getAll(team.id);
    }

    if(RoleConstants.NHOMROLENHANVIEN.includes(this.currentUser.role) || this.currentUser.role == RoleConstants.PHOTRUONGPHONG)  {
      this.listNhanvien = [];

      this.listNhanvien.push({
        accountName: this.currentUser.fullName,
        accountId: this.currentUser.id
      });

      this.nhanvien = this.currentUser.id;

      // lấy tên trưởng phòng của nhân viên
      let teamAcc = await this.teamAccService.getbyAccountId(this.currentUser.id);
      let team = await this.teamService.getFetchById(teamAcc.teamId);
      this.idLeader = team.idLeader;
    }

  }

  // selection change event
  public async getListNhanvienByTeam(idTeam) {
    this.listNhanvien = await this.teamAccService.getAll(idTeam);
    this.idLeader = (this.listTeam.filter(a => a.id == idTeam)[0]).idLeader;
  }

  // danh sách phòng ban
  public async getListTeam() {
    this.listTeam = await this.teamService.getFetch();
  }

  // danh sách acc
  public async getListAccount() {
    this.listAccount = await this.userService.getFetchAccount();
  }
  // xem chi tiết
  view(item) {
    if(item.id) {
      item.viewonly = true;
      this.mDialog.setDialog(
        this,
        DetailTaskComponent,
        "",
        "",
        item,
        "60%",
        "90vh",
        1
      );
      this.mDialog.open();
    }

  }

  // báo cáo
  report() {
    this.listIssue = [];
    let dateFrom = "";
    let dateTo = "";

    if (this.timeFrom && this.timeTo) {
      dateFrom = this.datePipe.transform(
        this.timeFrom,
        "MM-dd-yyyy"
      );

      dateTo = this.datePipe.transform(
        this.timeTo,
        "MM-dd-yyyy"
      );
    }

    let temp = [];
    this.issueService.getReportCanhan(this.nhanvien, dateFrom, dateTo).subscribe(res => {
      let modelIssue: BaoCaoIssue = {
        stt: 'I',
        title: 'CÔNG VIỆC THỰC HIỆN',
        dueDate: '',
        finishDate: '',
        coopId: '',
        idLeader: '',
        hqCao: '',
        hqDat: '',
        hqThap: '',
        hqChuadambao: '',
        tdVuot: '',
        tdDung: '',
        tdCham: '',
        nnKhachquan: '',
        nnDieuchinh: '',
        nnChuquan: '',
      }
      temp.push(modelIssue);

      // phân nhóm theo loại công việc phát sinh hay không
      const grouped = this.groupBy(res, item => item.isRisingwork);
      if (grouped.false) {
        temp = temp.concat(grouped.false);

      }

      let model2: BaoCaoIssue = {
        stt: 'II',
        title: 'CÔNG VIỆC PHÁT SINH',
        dueDate: '',
        finishDate: '',
        coopId: '',
        idLeader: '',
        hqCao: 'X',
        hqDat: '',
        hqThap: '',
        hqChuadambao: '',
        tdVuot: '',
        tdDung: '',
        tdCham: '',
        nnKhachquan: '',
        nnDieuchinh: '',
        nnChuquan: '',
      }

      temp.push(model2);
      if (grouped.true) {
        temp = temp.concat(grouped.true);

      }


      let countStt = 1;

      temp.forEach(item => {
        if (item.stt != 'I' && item.stt != 'II') {
          item.stt = countStt;
          countStt++;
        }
      });

      this.listIssue = temp;
      if (res.length > 0) {
        this.commonService.showeNotiResult("Lấy dữ liệu báo cáo thành công !", 2000);
      } else {
        this.commonService.showeNotiResult("Không có công việc cho cá nhân này !", 2000);
      }
      this.dataSource = new MatTableDataSource(this.listIssue);
    });
  }

  groupBy(xs, f) {
    return xs.reduce((r, v, i, a, k = f(v)) => ((r[k] || (r[k] = [])).push(v), r), {});
  }
}
