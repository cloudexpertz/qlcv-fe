import { async } from '@angular/core/testing';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BaseRepositoryService } from './baserepository.service';


@Injectable({
    providedIn: 'root'
})
export class MenuIssueService {

    serviceName = "menu-issue";

    constructor(private repository: BaseRepositoryService) {
    }
    // lấy công việc theo menu
    public getByMenu(idAccount: string, menu:any, timeFrom?: any, timeTo?: any, isRisingWork?: any, idTeam:any = "", pageSize: number = 0, pageNumber: number = 0) {
        return this.repository.getRepository(this.serviceName + '?accountId=' + idAccount + '&menu=' + menu +
        '&timeFrom=' + timeFrom + '&timeTo=' + timeTo + '&isRisingwork=' + isRisingWork + '&idTeam='+ idTeam + '&pageSize=' + pageSize + '&pageNumber=' + pageNumber);
    }
}
