import { RoleConstants } from 'src/app/shared/constants';
import { UserService } from 'src/app/services/user.services';
import { TeamService } from './../../../services/team.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RoleService } from 'src/app/services/role.service';
import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { ProgressService } from 'src/app/shared/services/progress-http.service';
import { validationAllErrorMessagesService, displayFieldCssService } from 'src/app/shared/validators/validatorService';
import { RoleIoComponent } from '../../roles/role-io/role-io.component';

@Component({
  selector: 'app-team-io',
  templateUrl: './team-io.component.html',
  styleUrls: ['./team-io.component.css']
})
export class TeamIoComponent implements OnInit {

  IOForm: FormGroup;
  public inputModel: any;
  public editMode: boolean;
  public purpose: string;
  public obj: any;
  dataModel: any;
  // error message
  validationErrorMessages = {
    name: { required: "Tên phòng ban không được để trống!" },
  };

  // form errors
  formErrors = {
    name: "",
  };
  listLeader: any;
  // ctor
  constructor(
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public imDialogService: MatdialogService,
    private teamService: TeamService,
    private commonService: CommonServiceShared,
    public dialogRef: MatDialogRef<RoleIoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserService

  ) { }

  // onInit
  ngOnInit() {
    if (this.data.model && this.data.model.purpose) {
      this.purpose = this.data.model.purpose;
      this.dataModel = this.data.model
    }
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }
  // Lấy danh sách leader
  async getLeaderList() {
    this.listLeader = await this.userService.getAccountByRole(RoleConstants.NHOMTRUONGPHONG);
  }
  // config Form use add or update
  bindingConfigAddOrUpdate() {
    this.getLeaderList();
    // check edit
    if (this.dataModel) {
      this.IOForm.setValue({
        name: this.dataModel.name,
        projectId: this.dataModel.projectId,
        idLeader: this.dataModel.idLeader,
      });
    }
  }

  // config input validation form
  bindingConfigValidation() {
    this.getLeaderList();
    this.IOForm = this.formBuilder.group({
      name: ["", Validators.required],
      projectId: [""],
      idLeader: [""],
    });
  }


  // on Submit
  public onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.IOForm.valid === true && !this.purpose) {
      this.inputModel = this.IOForm.value;
      this.teamService.insert(this.inputModel).subscribe(
        res => this.imDialogService.doParentFunction("getTeam"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Thêm mới phòng ban/nhóm thành công!",
            2000
          )
      );
      this.imDialogService.doParentFunction("closeMatDialog");
    } if (this.IOForm.valid === true && this.purpose) {
      this.inputModel = this.IOForm.value;
      this.inputModel.id = this.dataModel.id;
      this.teamService.updatet(this.inputModel).subscribe(
        res => this.imDialogService.doParentFunction("getTeam"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Cập nhật phòng ban/nhóm thành công!",
            2000
          )
      );
      this.imDialogService.doParentFunction("closeMatDialog");
    }


  }

  // add or update continue
  public addOrUpdateContinue(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.IOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.onFormReset();
      this.purpose = "new";
    }
  }

  // add or update
  private addOrUpdate(operMode: string) {

  }

  // on form reset
  public onFormReset() {
    this.IOForm.reset();
  }
  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.IOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // display fields css
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }

  // close sidebar
  public closeIOSidebar() {
    this.imDialogService.doParentFunction("closeMatDialog");
  }


}
