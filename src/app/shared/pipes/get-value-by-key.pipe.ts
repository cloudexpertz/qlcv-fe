import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "GetValueByKeyPipe",
})
export class GetValueByKeyPipe implements PipeTransform {
  transform(value: any, keyTag: string, nameTag: string, arrObject: any): string {
    if (arrObject) {
      for (const item of arrObject) {
        if (item[keyTag] == value) {
          return item[nameTag];
        }
      }
    }
    return '';
  }
}
