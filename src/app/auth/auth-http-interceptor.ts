import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from "@angular/router";
import { CommonServiceShared } from '../shared/services/common-services';
import { ProgressService } from '../shared/services/progress-http.service';

@Injectable()
export class ProgressHttpInterceptor implements HttpInterceptor {

  constructor(
    private progressService: ProgressService,
    private router: Router,
    public commonService: CommonServiceShared
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next
      .handle(req)
      .pipe(
        tap((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
;
          }
        }, (error) => {
          if (error.status === 401) {
            localStorage.removeItem('token');
            this.router.navigateByUrl('/login');
            this.commonService.showeNotiResult("Phiên làm việc của bạn đã hết. Vui lòng đăng nhập lại", 5000);
          }
          if (error.status === 403) {
            this.commonService.showeNotiResult("Bạn không có quyền sử dụng chức năng này", 2000);
          }
        })
      );
  }
}
