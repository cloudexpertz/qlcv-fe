import { async } from '@angular/core/testing';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BaseRepositoryService } from './baserepository.service';


@Injectable({
    providedIn: 'root'
})
export class ProjectService {

    serviceName = "project";

    constructor(private repository: BaseRepositoryService) {
    }


    // Lấy danh sách nhiệm vụ chuyên môn
    public getNvChuyenMon(idAccount, menu) {
        return this.repository.getRepository(this.serviceName + '/nvchuyenmon?idAccount=' + idAccount + '&menu=' + menu);
    }

    // Thêm mới nhiệm vụ chuyên môn
    public addNvChuyenMon(obj: any) {
        return this.repository.addRepository(this.serviceName + '/nvchuyenmon', obj);
    }

    // Lấy danh sách dự án
    public getDuan(idAccount) {
        return this.repository.getRepository(this.serviceName + '/duan?idAccount=' + idAccount);
    }

    // Thêm mới dự án
    public addDuan(obj: any) {
        return this.repository.addRepository(this.serviceName + '/duan', obj);
    }

    // Update chung
    public update(obj: any) {
        return this.repository.updateRepository(this.serviceName, obj);
    }

    // delete chung
    public delete(id: string) {
        return this.repository.deleteRepository(this.serviceName + "?id=" + id);
    }

    // Lấy project theo id
    public getProjectById(id: string) {
        return this.repository.getRepository(this.serviceName + '/get-by-id' + "?id=" + id);
    }

    // Lấy danh sách project
    public getProjectds() {
        var response = this.repository.getFetchRepository(this.serviceName);
        return response;
    }

    public async getFetchNvChuyenMon(idAccount) {
        return await this.repository.getFetchRepository(this.serviceName + '/nvchuyenmon?idAccount=' + idAccount);
    }

    // Lấy danh sách dự án
    public async getFetchDuan(idAccount) {
        return await this.repository.getFetchRepository(this.serviceName + '/duan?idAccount=' + idAccount);
    }

    // Lấy danh sách hoạt động
    public async getAction(projectId: string) {
        return await this.repository.getFetchRepository(this.serviceName + '/get-action?projectId=' + projectId);
    }
}
