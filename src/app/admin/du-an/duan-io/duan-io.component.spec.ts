import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuanIoComponent } from './duan-io.component';

describe('DuanIoComponent', () => {
  let component: DuanIoComponent;
  let fixture: ComponentFixture<DuanIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuanIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuanIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
