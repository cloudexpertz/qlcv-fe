import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueIoComponent } from './issue-io.component';

describe('IssueIoComponent', () => {
  let component: IssueIoComponent;
  let fixture: ComponentFixture<IssueIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssueIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
