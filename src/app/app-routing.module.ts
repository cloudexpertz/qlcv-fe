import { MembersComponent } from './members/members.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/guardauthen';
import { LoginComponent } from './auth/login/login.component';
import { LayoutComponent } from './shared/layout/layout.component';


const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    {
        path: 'admin',
        redirectTo: '/admin/dashboard',
        pathMatch: 'full'
    },
    {
        path: 'admin',
        loadChildren: './admin/admin.module#AdminModule',
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: '/admin/dashboard',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule],
    providers: []
})
export class AppRoutingModule { }
