import { RoleService } from './../../services/role.service';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'RolelabelPipe' })
export class RolelabelPipe implements PipeTransform {
    /**
     *
     */
    constructor(private roleService: RoleService) {}
    async transform(rolename: any) {

        var role = await this.roleService.getByName(rolename);

        return role.roleLabel;
    }
}