import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BaseRepositoryService } from './baserepository.service';


@Injectable({
    providedIn: 'root'
})
export class RoleService {

    serviceName = "role";

    constructor(private repository: BaseRepositoryService) {
    }

    // Lấy danh sách role
    public getRole() {
        return this.repository.getRepository(this.serviceName + "/label");
    }
    public  getFetchRole() {
        var response =  this.repository.getFetchRepository(this.serviceName + "/label");
        return  response ;
    }

    // Thêm role
    public insertRole(obj: any) {
        return this.repository.addRepository(this.serviceName, obj);
    }

    // Update role
    public updateRole(obj: any) {
        return this.repository.updateRepository(this.serviceName, obj);
    }

    // getByName
    public  getByName(name: string) {
      var response = this.repository.getFetchRepository(this.serviceName + '/get-by-name?name='+ name);
      
      if(response) {
        return ( response) ;
      }
    }
}
