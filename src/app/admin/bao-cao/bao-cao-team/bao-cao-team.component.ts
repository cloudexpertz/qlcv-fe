import { RoleConstants } from 'src/app/shared/constants';
import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { IssueService } from 'src/app/services/issue.service';
import { ProjectService } from 'src/app/services/project.service';
import { TeamAccountService } from 'src/app/services/team-account.service';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.services';
import { CommonServiceShared } from 'src/app/shared/services/common-services';

@Component({
  selector: 'app-bao-cao-team',
  templateUrl: './bao-cao-team.component.html',
  styleUrls: ['./bao-cao-team.component.css']
})
export class BaoCaoTeamComponent implements OnInit {
  @ViewChild('TABLE', { static: true }) table: ElementRef;
  displayedColumns: string[] = ['position', 'name', 'cao', 'dat', 'thap', 'chua-dam-bao', 'tong-so-congviec', 'vuot', 'dung', 'cham', 'khach-quan', 'dieu-chinh', 'chu-quan',
    'chaphanh-ot', 'khong-chaphanh-ot', 'chaphanh-ttbc', 'khong-chaphanh-ttbc', 'ghi-chu'];
  dataSource: any;
  team: any;
  // ngModel
  public projectId: any;
  public timeFrom: any;
  public timeTo: any;

  public listTeam: any;
  public listIssue: any = [];
  public currentUser: any;
  public listAccount: any = [];

  constructor(
    public teamService: TeamService,
    public teamAccService: TeamAccountService,
    public datePipe: DatePipe,
    private issueService: IssueService,
    private userService: UserService,
    private commonService: CommonServiceShared
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentAcc'));

    this.getListTeam();
    this.getListAccount();
  }

  // export table ra file excel
  ExportTOExcel() {
    this.commonService.ExportTOExcel(this.table, 'bao-cao-phong-ban');
  }

  // danh sách phòng ban
  public async getListTeam() {
    this.listTeam = await this.teamService.getFetch();

    if(!RoleConstants.NHOMROLEGIAMDOC.includes(this.currentUser.role) && this.currentUser.role != RoleConstants.PHUTRACHTONGHOP &&  !RoleConstants.NHOMROLENHANVIEN.includes(this.currentUser.role)) {
      this.listTeam = this.listTeam.filter(a => a.idLeader == this.currentUser.id);
    } 

    if(RoleConstants.NHOMROLENHANVIEN.includes(this.currentUser.role)) {
      let teamAcc = await this.teamAccService.getbyAccountId(this.currentUser.id);

      this.listTeam = this.listTeam.filter(a => a.id == teamAcc.teamId);
    }
  }

  // danh sách acc
  public async getListAccount() {
    this.listAccount = await this.userService.getFetchAccount();
  }

  // báo cáo
  report() {
    this.listIssue = [];
    let dateFrom = "";
    let dateTo = "";

    if (this.timeFrom && this.timeTo) {
      dateFrom = this.datePipe.transform(
        this.timeFrom,
        "MM-dd-yyyy"
      );

      dateTo = this.datePipe.transform(
        this.timeTo,
        "MM-dd-yyyy"
      );
    }
    let accountId = "";
    if( RoleConstants.NHOMROLENHANVIEN.includes(this.currentUser.role)) {
      accountId = this.currentUser.id;
    }

    this.issueService.getReportByTeam(this.team, accountId, dateFrom, dateTo).subscribe(res => {
      res.map((item, index) => {
        item.stt = index + 1;
      });
      if (res.length > 0) {
        this.commonService.showeNotiResult("Lấy dữ liệu báo cáo thành công !", 2000);
      } else {
        this.commonService.showeNotiResult("Không có công việc cho nhiệm vụ/dự án này !", 2000);
      }

      this.dataSource = new MatTableDataSource(res);
    });
  }

}
