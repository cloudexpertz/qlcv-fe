import { AbstractControl, FormGroup } from '@angular/forms';

// validation number
export function numberValidator(control: AbstractControl): { [key: string]: any } | null {
  const valid = /^\d+$/.test(control.value);
  return valid ? null : { invalidNumber: { valid: false, value: control.value } };
}

// validation input message error
export function validationErrorMessagesService(form: FormGroup, validationErrorMessages: any, formErrors: any) {
  Object.keys(form.controls).forEach((key: string) => {
    const abstractControl = form.get(key);
    formErrors[key] = '';
    if (abstractControl && !abstractControl.valid && (abstractControl.touched || abstractControl.dirty)) {
      const message = validationErrorMessages[key];
      for (const errorKey in abstractControl.errors) {
        if (errorKey) {
          formErrors[key] += message[errorKey] + '';
        }
      }
    }
    if (abstractControl instanceof FormGroup) {
      validationErrorMessages(abstractControl);
    }
  });
}
// validation input message error
export function validationAllErrorMessagesService(form: FormGroup, validationErrorMessages: any, formErrors: any) {
  Object.keys(form.controls).forEach((key: string) => {
    const abstractControl = form.get(key);
    formErrors[key] = '';
    if (abstractControl && !abstractControl.valid) {
      const message = validationErrorMessages[key];
      for (const errorKey in abstractControl.errors) {
        if (errorKey) {
          formErrors[key] += message[errorKey] + '';
        }
      }
    }
    if (abstractControl instanceof FormGroup) {
      validationErrorMessages(abstractControl);
    }
  });
}
// display fields css
export function displayFieldCssService(field: string) {
  return {
    'has-error': field,
    'has-feedback': field
  };
}


export class ValidatorToaDoService {
  public errorX = '';
  public errorY = '';
  public errorSrid = '';
  public errorKieuToaDo = '';
  public submit = false;
  constructor() { }

  public resetValidatorToaDo() {
    this.errorX = '';
    this.errorY = '';
    this.errorSrid = '';
    this.errorKieuToaDo = '';
    this.submit = false;
  }
  public validatorToaDo(form: FormGroup) {
    this.resetValidatorToaDo();
    if ((!form.value.toadox === true) && (!form.value.toadoy === true)) {
      if (form.valid === true) { this.submit = true; }
    } else {
      if ((!form.value.toadox === true) || (!form.value.toadoy === true) || (!form.value.srid === true)) {
        (!form.value.toadox === true) ? this.errorX = "Bạn phải nhập tọa độ x" : this.errorX = "";
        (!form.value.toadoy === true) ? this.errorY = "Bạn phải nhập tọa độ y" : this.errorY = "";
        (!form.value.srid === true) ? this.errorSrid = "Bạn phải chọn srid" : this.errorSrid = "";
        (!form.value.kieutoado === true) ? this.errorKieuToaDo = "Bạn phải chọn kiểu tọa độ" : this.errorKieuToaDo = "";
      } else {
        if (form.valid === true) { this.submit = true; }
      }
    }
    if (form.value.kieutoado === 'xy') {
      if ((!form.value.toadox === true) && (!form.value.toadoy === true)) {
        if (form.valid === true) { this.submit = true; }
      } else {
        if ((!form.value.toadox === true) || (!form.value.toadoy === true)
          || (!form.value.srid === true)) {
          (!form.value.toadox === true) ? this.errorX = "Bạn phải nhập tọa độ x" : this.errorX = "";
          (!form.value.toadoy === true) ? this.errorY = "Bạn phải nhập tọa độ y" : this.errorY = "";
          (!form.value.srid === true) ? this.errorSrid = "Bạn phải chọn srid" : this.errorSrid = "";
          (!form.value.kieutoado === true) ? this.errorKieuToaDo = "Bạn phải chọn kiểu tọa độ" : this.errorKieuToaDo = "";
        } else {
          if (form.valid === true) { this.submit = true; }
        }
      }
    } else if (form.value.kieutoado === 'latlng') {
      if ((!form.value.toadox === true) && (!form.value.toadoy === true)) {
        if (form.valid === true) { this.submit = true; }
      } else {
        if ((!form.value.toadox === true) || (!form.value.toadoy === true)) {
          (!form.value.toadox === true) ? this.errorX = "Bạn phải nhập latitude" : this.errorX = "";
          (!form.value.toadoy === true) ? this.errorY = "Bạn phải nhập longitude" : this.errorY = "";
          (!form.value.srid === true) ? this.errorSrid = "Bạn phải chọn srid" : this.errorSrid = "";
          (!form.value.kieutoado === true) ? this.errorKieuToaDo = "Bạn phải chọn kiểu tọa độ" : this.errorKieuToaDo = "";
        } else {
          if (form.valid === true) { this.submit = true; }
        }
      }
    }
  }
}
