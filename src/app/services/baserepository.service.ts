import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class BaseRepositoryService {
  private headers: HttpHeaders;
  private accessPointUrl = environment.backendUrl;

  // ctor
  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders(
      {
        'Content-Type': 'application/json; charset=utf-8',
         Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    );

  }

  // get all
  public  getRepository(serviceName: string) {
    return this.http.get<any>(this.accessPointUrl + serviceName, { headers: this.headers });
  }

  // get use fetch
  public getFetchRepository(url: string) {
    const response = this.http.get<any>(this.accessPointUrl + url, { headers: this.headers }).toPromise();
    return response;
  }

  // add
  public addRepository(serviceName: string, body: any) {
    return this.http.post(this.accessPointUrl + serviceName, body, { headers: this.headers });
  }

  // update
  public updateRepository(serviceName: string, body: any) {
    return this.http.put(this.accessPointUrl + serviceName , body, { headers: this.headers });
  }

  // delete
  public deleteRepository(serviceName: string) {
    return this.http.delete(this.accessPointUrl + serviceName , { headers: this.headers });
  }

  // get use fetch by id
  public getFetchRepositoryId(url: string, id : number) {
    const response = fetch(this.accessPointUrl + url + '/' +id);
    return response;
  }

  uploadAndProgress(url, files: File[]){
    var header = new HttpHeaders(
      {
         Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    );
    var formData = new FormData();
    Array.from(files).forEach(f => formData.append('file',f))
    
    return this.http.post(this.accessPointUrl + url, formData, {reportProgress: true, observe: 'events', headers: header });
  }
}
