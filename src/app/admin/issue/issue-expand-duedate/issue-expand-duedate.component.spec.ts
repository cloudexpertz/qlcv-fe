import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueExpandDuedateComponent } from './issue-expand-duedate.component';

describe('IssueExpandDuedateComponent', () => {
  let component: IssueExpandDuedateComponent;
  let fixture: ComponentFixture<IssueExpandDuedateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssueExpandDuedateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueExpandDuedateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
