import { BaoCaoCongViecComponent } from './bao-cao/bao-cao-cong-viec/bao-cao-cong-viec.component';
import { LinhVucComponent } from './linh-vuc/linh-vuc.component';
import { BaoCaoTeamComponent } from './bao-cao/bao-cao-team/bao-cao-team.component';
import { BaoCaoComponent } from './bao-cao/bao-cao.component';
import { BaoCaoCaNhanComponent } from './bao-cao/bao-cao-ca-nhan/bao-cao-ca-nhan.component';
import { TaskSubtaskComponent } from './task-subtask/task-subtask.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard, GiamDocGuard } from "../auth/guardauthen";
import { MembersComponent } from "../members/members.component";
import { AdminComponent } from "./admin.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { DuAnComponent } from "./du-an/du-an.component";
import { NvChuyenmonComponent } from "./nv-chuyenmon/nv-chuyenmon.component";
import { RolesComponent } from "./roles/roles.component";
import { TeamsComponent } from "./teams/teams.component";
import { IssueComponent } from './issue/issue.component';
import { ProfileComponent } from './profile/profile.component';

const appRoutes: Routes = [
    {
        path: "",
        component: AdminComponent,
        children: [
            {
                path: 'profile',
                component: ProfileComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'quantri',
                children: [
                    { path: 'teams', component: TeamsComponent },
                    { path: 'role', component: RolesComponent },
                    { path: 'member', component: MembersComponent },
                    { path: 'linhvuc', component: LinhVucComponent },
                ],
                canActivate: [GiamDocGuard]
            },
            {
                path: 'dashboard',
                component: DashboardComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'nvchuyenmon/1',
                component: NvChuyenmonComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'nvchuyenmon/2',
                component: NvChuyenmonComponent,
                canActivate: [AuthGuard]
            },

            {
                path: 'duan',
                component: DuAnComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'task',
                component: TaskSubtaskComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'issue-chuyenmon',
                component: IssueComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'issue/1',
                component: IssueComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'issue/0',
                component: IssueComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'issue-duan',
                component: IssueComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'member', component: MembersComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'baocao',
                children: [
                    { path: 'canhan', component: BaoCaoCaNhanComponent },
                    { path: 'nhiemvu', component: BaoCaoComponent },
                    { path: 'duan', component: BaoCaoComponent },
                    { path: 'team', component: BaoCaoTeamComponent },
                    { path: 'congviec', component: BaoCaoCongViecComponent }
                ],
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                redirectTo: '',
                pathMatch: 'full'
            }]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(appRoutes)
    ],
    exports: [RouterModule],
    providers: []
})
export class AdminRoutingModule { }
