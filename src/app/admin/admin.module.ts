import { MatTableExporterModule } from 'mat-table-exporter';
import { DatePipe, TitleCasePipe } from "@angular/common";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { CoreModule } from "@angular/flex-layout";
import { DateAdapter, MatFormFieldModule, MatInputModule, MatPaginatorModule, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from "@angular/material";
import { ProgressHttpInterceptor } from "../auth/auth-http-interceptor";
import { LoginComponent } from "../auth/login/login.component";
import { MemberIoComponent } from "../members/member-io/member-io.component";
import { MembersComponent } from "../members/members.component";
import { UserService } from "../services/user.services";
import { CommonServiceShared } from "../shared/services/common-services";
import { MatdialogService } from "../shared/services/mat-dialog.service";
import { ProgressService } from "../shared/services/progress-http.service";
import { SharedModule } from "../shared/shared.module";
import { AdminRoutingModule } from "./admin-routing.module";
import { AdminComponent } from "./admin.component";
import { DashboardComponent } from './dashboard/dashboard.component';
import { TeamsComponent } from './teams/teams.component';
import { RolesComponent } from './roles/roles.component';
import { RoleIoComponent } from './roles/role-io/role-io.component';
import { RolelabelPipe } from "../shared/pipes/role-label.pipe";
import { TeamIoComponent } from './teams/team-io/team-io.component';
import { TeamMemberComponent } from './teams/team-member/team-member.component';
import { CustomMaterialModule } from "../custom-material/custom-material.module";
import { NvChuyenmonComponent } from "./nv-chuyenmon/nv-chuyenmon.component";
import { NvChuyenmonIoComponent } from './nv-chuyenmon/nv-chuyenmon-io/nv-chuyenmon-io.component';
import { UserlabelPipe } from "../shared/pipes/user-label.pipe";
import { DuAnComponent } from "./du-an/du-an.component";
import { DuanIoComponent } from './du-an/duan-io/duan-io.component';
import { TaskSubtaskComponent } from './task-subtask/task-subtask.component';
import { AngularSplitModule } from "angular-split";
import {MatListModule} from '@angular/material/list';
import { AccountlabelPipe } from "../shared/pipes/account-label.pipe";
import { TeamlabelPipe } from "../shared/pipes/team-label.pipe";
import { TaskIoComponent } from './task-subtask/task-io/task-io.component';
import { IssueComponent } from './issue/issue.component';
import { IssueIoComponent } from './issue/issue-io/issue-io.component';
import { GetNameByIdPipe } from "../shared/pipes/id2name.pipe";
import { GetValueByKeyPipe } from "../shared/pipes/get-value-by-key.pipe";
import { DetailTaskComponent } from './detail-task/detail-task.component';
import { NoteBoxComponent } from './detail-task/note-box/note-box.component';
import { BaoCaoComponent } from './bao-cao/bao-cao.component';
import { BaoCaoCaNhanComponent } from './bao-cao/bao-cao-ca-nhan/bao-cao-ca-nhan.component';
import { GetNameByArrayIdPipe } from "../shared/pipes/arrayId2Name.pipe";
import { BaoCaoTeamComponent } from './bao-cao/bao-cao-team/bao-cao-team.component';
import { LinhVucComponent } from './linh-vuc/linh-vuc.component';
import { LinhVucIoComponent } from './linh-vuc/linh-vuc-io/linh-vuc-io.component';
import { BaoCaoCongViecComponent } from './bao-cao/bao-cao-cong-viec/bao-cao-cong-viec.component';
import { ChartModule } from 'angular-highcharts';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { ProfileComponent } from './profile/profile.component';
import { GiamDocGuard } from '../auth/guardauthen';
import { IssueExpandDuedateComponent } from './issue/issue-expand-duedate/issue-expand-duedate.component';
export const MY_FORMATS = {
  parse: {
      dateInput: 'LL'
  },
  display: {
      dateInput: 'DD/MM/YYYY',
      monthYearLabel: 'YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'YYYY'
  }
};
@NgModule({
  declarations: [
    AdminComponent,
    LoginComponent,
    MembersComponent,
    MemberIoComponent,
    DashboardComponent,
    TeamsComponent,
    RolesComponent,
    RoleIoComponent,
    RolelabelPipe,
    UserlabelPipe,
    TeamIoComponent,
    TeamMemberComponent,
    NvChuyenmonComponent,
    NvChuyenmonIoComponent,
    DuAnComponent,
    DuanIoComponent,
    TaskSubtaskComponent,
    AccountlabelPipe,
    TeamlabelPipe,
    TaskIoComponent,
    IssueComponent,
    IssueIoComponent,
    GetNameByIdPipe,
    GetValueByKeyPipe,
    DetailTaskComponent,
    NoteBoxComponent,
    BaoCaoComponent,
    BaoCaoCaNhanComponent,
    GetNameByArrayIdPipe,
    BaoCaoTeamComponent,
    LinhVucComponent,
    LinhVucIoComponent,
    BaoCaoCongViecComponent,
    ProfileComponent,
    IssueExpandDuedateComponent
  ],
  imports: [
    CoreModule,
    SharedModule,
    CustomMaterialModule.forRoot(),
    AdminRoutingModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    AngularSplitModule,
    MatListModule,
    MatTableExporterModule,
    ChartModule ,
  ],

  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    UserService,
    CommonServiceShared,
    TitleCasePipe,
    ProgressService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ProgressHttpInterceptor,
      multi: true
    },
    MatdialogService,
    DatePipe,
    GiamDocGuard

  ],
  entryComponents: [
    MemberIoComponent,
    RoleIoComponent,
    TeamIoComponent,
    TeamMemberComponent,
    DuanIoComponent,
    NvChuyenmonIoComponent,
    TaskIoComponent,
    IssueIoComponent,
    DetailTaskComponent,
    NoteBoxComponent,
    LinhVucIoComponent,
    IssueExpandDuedateComponent
  ],
  bootstrap: []
})
export class AdminModule { }
