import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { UserService } from 'src/app/services/user.services';
import { IssueService } from 'src/app/services/issue.service';
import { async } from '@angular/core/testing';
import { ProjectService } from 'src/app/services/project.service';
import { SettingService } from './../../services/setting.service';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { MatdialogService } from 'src/app/shared/services/mat-dialog.service';
import { NoteBoxComponent } from './note-box/note-box.component';
import { HttpErrorResponse } from '@angular/common/http';
import { RoleConstants } from 'src/app/shared/constants';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-detail-task',
  templateUrl: './detail-task.component.html',
  styleUrls: ['./detail-task.component.css']
})
export class DetailTaskComponent implements OnInit {
  listTrangthai: any;
  listMucdo: any;
  listChatluong:any;
  listTiendo: any;
  listSettingAction: any;
  listProject: any;
  listSubTask: any;
  listAccount: any;
  listAction: any;
  listComment: any;
  // ngModel
  trangthai ;
  mucdo ;
  chatluong: any;
  tiendo: any;
  comment: any;
  RoleConstants = RoleConstants;
  listChaphanhSetting = [
    {
      key: 'Chấp hành',
      value: 1
    },
    {
      key: 'Không chấp hành',
      value: 0
    }
  ];
  chaphanh_ttbc: any;
  chaphanh_ot: any;

  dataModel:any;
  currentUser: any;
  viewonly: any;

  fileUrlPath = "";
  // dialog
  public mDialog: any;
  constructor(
    private settingService: SettingService,
    private imDialog: MatDialog,
    public imDialogService: MatdialogService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public projectService: ProjectService,
    public issueService: IssueService,
    public userService: UserService,
    public commonService: CommonServiceShared
  ) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
   }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentAcc'));
    this.dataModel = this.data.model
    this.viewonly = this.dataModel.viewonly;
    this.fileUrlPath = this.dataModel.fileUrl ? environment.rootPath + this.dataModel.fileUrl : "Không có";
    this.getProject();
    this.getCombobox();
    this.getSubtask();
    this.getAction();
    this.getListAccount();
    this.getComment();

    // ngModel init
    this.trangthai = this.dataModel.status;
    this.mucdo = this.dataModel.priority;
    this.chatluong = this.dataModel.ratingQuality;
    this.tiendo = this.dataModel.rateProgress;
    this.chaphanh_ttbc = this.dataModel.isChaphanhTtbc;
    this.chaphanh_ot = this.dataModel.isChaphanhOt;
  }
  // Kiểm tra current user có phải coop của công việc không
  public checkCoop(element: any) {
    if (element.coopId.includes(this.currentUser.id)) {
      return true;
    }
    return false;
  }
  //reset button toggle khi ấn hủy dialog
  async resetButtonToggle() {
    this.listTiendo = await this.settingService.getFetchSetting('0', 'tiendo');
    this.listChatluong = await this.settingService.getFetchSetting('0', 'chatluong');
    this.chatluong = this.dataModel.ratingQuality;
    this.tiendo = this.dataModel.rateProgress;

  }
  // lấy json trạng thái
  async getCombobox() {
    this.listTrangthai = await this.settingService.getFetchSetting('0', 'trangthai');
    this.listMucdo = await this.settingService.getFetchSetting('0', 'priority');
    this.listChatluong = await this.settingService.getFetchSetting('0', 'chatluong');
    this.listTiendo = await this.settingService.getFetchSetting('0', 'tiendo');
    this.listSettingAction = await this.settingService.getFetchSetting('0', 'action');
  }

  ChangeTrangthai(item) {
    var data = {
      model: this.dataModel,
      key: 'status',
      value: item.value,
      openNote: false
    }

    if(RoleConstants.NHOMROLENHANVIEN.includes(this.currentUser.role)) {
      data.openNote = true;
      this.showPopup(data, '23vh');
    } else {
      this.showPopup(data, '15vh');
    }

  }

  changeMucdo(item) {
    var data = {
      model: this.dataModel,
      key: 'priority',
      value: item.value,
      openNote: false
    }

    this.showPopup(data, '15vh');

  }

  rateChatluong(item) {
    var data = {
      model: this.dataModel,
      key: 'ratingQuality',
      value: item,
      openNote: false
    }

    this.showPopup(data, '15vh');

  }

  rateTiendo(item) {
    var data = {
      model: this.dataModel,
      key: 'rateProgress',
      value: item,
      isLate: false,
      openNote: true
    }
    if(item == 0) {
      data.isLate = true;
      this.showPopup(data, '26vh');
    } else {
      this.showPopup(data, '23vh');
    }

  }
  rateChaphanhTTBC(item) {
    var data = {
      model: this.dataModel,
      key: 'isChaphanhTtbc',
      value: item,
      openNote: false
    }

    this.showPopup(data, '15vh');
  }
  rateChaphanhOT(item) {
    var data = {
      model: this.dataModel,
      key: 'isChaphanhOt',
      value: item,
      openNote: false
    }

    this.showPopup(data, '15vh');
  }

  confirm(value) {
    var data = {
      model: this.dataModel,
      key: 'isConfirm',
      value: value,
      isConfirmAction: true,
      openNote: true
    }

    this.showPopup(data, '23vh');
  }
  // lấy dánh sách project
  public async getProject() {
    this.listProject = await this.projectService.getProjectds();
  }

  public async getSubtask() {
    this.listSubTask = await this.issueService.getSubtask(this.dataModel.id);
  }

  public async getAction() {
    this.listAction = await this.issueService.getAction(this.dataModel.id);
  }


  public async getListAccount() {
    this.listAccount = await this.userService.getFetchAccount();
  }

  public async getComment() {
    this.listComment = await this.issueService.getComment(this.dataModel.id);
  }
  //
  postComment() {
    if(this.comment && this.comment != '') {
      let commentModel = {
        note: this.comment,
        userActionId: this.currentUser.id,
        issueId: this.dataModel.id
      }
      this.issueService.postComment(commentModel).subscribe(
        res => this.getComment(),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Thêm bình luận thành công",
            2000
          )
      );
    }
    this.comment = "";
  }
  public showPopup(data?, height?) {
    this.mDialog.setDialog(
      this,
      NoteBoxComponent,
      "",
      "",
      data,
      "30%",
      height,
      1
    );
    this.mDialog.open();
  }

    /**
 * Hàm đóng mat dialog
 */
     closeMatDialog() {
      this.imDialog.closeAll();
    }
  
    doFunction(methodName) {
      this[methodName]();
    }
}
