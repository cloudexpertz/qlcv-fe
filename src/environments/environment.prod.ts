
 //const root = 'http://113.160.187.187:1111';

const root = 'https://localhost:5001';

//20.42.96.57
//132.226.5.110
export const environment = {
  production: true,
  rootPath: root,
  backendUrl: root + '/api/'
};
