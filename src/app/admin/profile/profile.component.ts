import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, RequiredValidator, Validators } from '@angular/forms';
import { RoleService } from 'src/app/services/role.service';
import { UserService } from 'src/app/services/user.services';
import { CommonServiceShared } from 'src/app/shared/services/common-services';
import { validationAllErrorMessagesService } from 'src/app/shared/validators/validatorService';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  IOForm: FormGroup;
  passwordForm: FormGroup;
  dataModel: any;
  currentUser: any = {};
  roleLabel: any;
  isChangePassword = false;
  inputModel:any;
  ischeckPassword = true;

  validationErrorMessages = {
    userName: { required: "Tên đăng nhập không được để trống!" },
    fullName: { required: "Họ và Tên không được để trống!" },
    password: { required: "Mật khẩu không được để trống!" },
  };

  // form errors
  formErrors = {
    userName: "",
    fullName: "",
    password: "",
  };

  constructor(private formBuilder: FormBuilder,
    private userService: UserService,
    private roleService: RoleService,
    private commonService: CommonServiceShared) { }

  ngOnInit() {

    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }

  // config Form use add or update
  async bindingConfigAddOrUpdate() {
    await this.getProfile();

    // check edit
    if (this.currentUser) {
      this.IOForm.setValue({
        fullName: this.currentUser.fullName,
        email: this.currentUser.email,
        sodienthoai: this.currentUser.sodienthoai,
        userName: this.currentUser.userName,
      });
    }
  }

  // lấy user hiện tại
  public async getProfile() {
    this.currentUser = await this.userService.getFetchProfile();
    this.roleLabel = await this.roleService.getByName(this.currentUser.role);

  }

  // config input validation form
  bindingConfigValidation() {

    this.IOForm = this.formBuilder.group({
      fullName: ["", Validators.required],
      email: [""],
      sodienthoai: [""],
      userName: [{ value: '', disabled: true }]
    });

    this.passwordForm = this.formBuilder.group({
      oldPassword: ["", Validators.required],
      newPassword: ["", Validators.required],
      retypePassword: ["", Validators.required],
    });
  };

  public openChangePassword() {
    this.isChangePassword = !this.isChangePassword;
  }

  public updateUserInfo() {
    this.logAllValidationErrorMessages();
    if (this.IOForm.valid === true) {
      this.inputModel = this.IOForm.value;
      this.userService.updatetCurrentUserInfo(this.inputModel).subscribe(
        res => this.getProfile(),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error.message);
        },
        () =>
          this.commonService.showeNotiResult(
            "Cập nhật thông tin thành công!",
            2000
          )
      );
    }
  }

  public updatePassword() {
    this.checkPassword();
    if (this.passwordForm.valid === true && this.ischeckPassword == true) {
      this.inputModel = this.passwordForm.value;
      this.userService.updatetCurrentUserPassword(this.inputModel).subscribe(
        res => {
          localStorage.removeItem('currentAcc');
          localStorage.removeItem('token');
          document.location.href = '/login';
          this.commonService.showeNotiResult(
            "Thay đổi mật khẩu thành công!",
            2000
          );
        },
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        }
      );
    }
  }
  // check password confirm
  public checkPassword() {

    if(this.passwordForm.controls["newPassword"].value != this.passwordForm.controls["retypePassword"].value) {
      this.ischeckPassword = false;
    } else {
      this.ischeckPassword = true;
    }

  }
  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.IOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }
}
