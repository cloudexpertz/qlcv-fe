import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "GetNameByIdPipe",
})
export class GetNameByIdPipe implements PipeTransform {
  transform(value: any, nameTag: string, arrObject: any): string {
    if (arrObject) {
      for (const item of arrObject) {
        if (item.id == value) {
          return item[nameTag];
        }
      }
    }
    return '';
  }
}