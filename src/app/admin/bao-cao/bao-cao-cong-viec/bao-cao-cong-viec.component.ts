import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { IssueService } from 'src/app/services/issue.service';
import { TeamAccountService } from 'src/app/services/team-account.service';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.services';
import { RoleConstants } from 'src/app/shared/constants';
import { CommonServiceShared } from 'src/app/shared/services/common-services';

@Component({
  selector: 'app-bao-cao-cong-viec',
  templateUrl: './bao-cao-cong-viec.component.html',
  styleUrls: ['./bao-cao-cong-viec.component.css']
})
export class BaoCaoCongViecComponent implements OnInit {
  @ViewChild('TABLE', { static: true }) table: ElementRef;
  displayedColumns: string[] = ['position', 'name','thoi-gian-hoan-thanh', 'ghi-chu', 'hoan-thanh', 'li-do', 'gia-han', 'kq-gia-han', 'danh-gia'];
  dataSource: any;
  team: any;
  // ngModel
  public projectId: any;
  public timeFrom: any;
  public timeTo: any;

  public listTeam: any;
  public listIssue: any = [];
  public currentUser: any;
  public listAccount: any = [];
  RoleConstants = RoleConstants;
  constructor(
    public teamService: TeamService,
    public teamAccService: TeamAccountService,
    public datePipe: DatePipe,
    private issueService: IssueService,
    private userService: UserService,
    private commonService: CommonServiceShared
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentAcc'));

    this.getListTeam();
    this.getListAccount();
  }
  // export table ra file excel
  ExportTOExcel() {
    this.commonService.ExportTOExcel(this.table, 'bao-cao-cong-viec');

  }
  // danh sách phòng ban
  public async getListTeam() {
    this.listTeam = await this.teamService.getFetch();

    if(!RoleConstants.NHOMROLEGIAMDOC.includes(this.currentUser.role) && this.currentUser.role != RoleConstants.PHUTRACHTONGHOP) {
      this.listTeam = this.listTeam.filter(a => a.idLeader == this.currentUser.id);
    } 
  }

  // danh sách acc
  public async getListAccount() {
    this.listAccount = await this.userService.getFetchAccount();
  }

  // báo cáo
  report() {
    this.listIssue = [];
    let dateFrom = "";
    let dateTo = "";

    if (this.timeFrom && this.timeTo) {
      dateFrom = this.datePipe.transform(
        this.timeFrom,
        "MM-dd-yyyy"
      );

      dateTo = this.datePipe.transform(
        this.timeTo,
        "MM-dd-yyyy"
      );
    }


    this.issueService.getReportCongviec(this.team, dateFrom, dateTo).subscribe(res => {

      if (res.length > 0) {
        this.commonService.showeNotiResult("Lấy dữ liệu báo cáo thành công !", 2000);
      } else {
        this.commonService.showeNotiResult("Không có công việc cho đơn vị này !", 2000);
      }

      this.dataSource = new MatTableDataSource(res);
    });
  }

}
